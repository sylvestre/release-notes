# Traditional Chinese messages for Debian Etch release notes.
# Copyright (C) 2005-2007 Free Software Foundation, Inc.
msgid ""
msgstr ""
"Project-Id-Version: Debian Lenny release notes\n"
"POT-Creation-Date: 2017-01-26 23:48+0100\n"
"PO-Revision-Date: 2007-04-01 19:02+0800\n"
"Last-Translator: Tetralet <tetralet@gmail.com>\n"
"Language-Team: Debian-user in Chinese [Big5] <debian-chinese-big5@lists."
"debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "zh_TW"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "更多關於 &debian; 的資訊"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "閱讀更多資訊"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
#, fuzzy
#| msgid ""
#| "Beyond these release notes and the installation guide, further "
#| "documentation on &debian; is available from the Debian Documentation "
#| "Project (DDP), whose goal is to create high-quality documentation for "
#| "Debian users and developers.  Documentation, including the Debian "
#| "Reference, Debian New Maintainers Guide, and Debian FAQ are available, "
#| "and many more.  For full details of the existing resources see the <ulink "
#| "url=\"&url-ddp;\">DDP website</ulink>."
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on &debian; is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers.  Available documentation includes the Debian Reference, Debian "
"New Maintainers Guide, the Debian FAQ, and many more.  For full details of "
"the existing resources see the <ulink url=\"&url-ddp;\">Debian Documentation "
"website</ulink> and the <ulink url=\"&url-wiki;\">Debian Wiki website</"
"ulink>."
msgstr ""
"除了發行情報和安裝指引之外，Debian 文件計畫 (DDP) 也提供了許多 &debian; 更進"
"一步的說明文件。Debian 文件計畫的目標是替 Debian 的使用者和開發者創作出高品質"
"的文件，其中包括了 Debian 參考手冊、Debian 新維護者指引、以及 Debian 常見問答"
"集，以及其他更多的文件。如果您想瞭解關於這些資源的完整訊息，請參考 <ulink "
"url=\"&url-ddp;\">DDP 站台</ulink>。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
#, fuzzy
#| msgid ""
#| "Documentation for individual packages is installed into <filename>/usr/"
#| "share/doc/<replaceable>package</replaceable></filename>.  This may "
#| "include copyright information, Debian specific details and any upstream "
#| "documentation."
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"個別套件的說明文件則會被安裝到 <filename>/usr/share/doc/"
"<replaceable>package</replaceable></filename> 目錄之中，其中可能會包含了版權"
"資訊，專屬於 Debian 的一些訊息，以及任何原作者所提供的說明文件。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "取得協助"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
#, fuzzy
#| msgid ""
#| "There are many sources of help, advice and support for Debian users, but "
#| "these should only be considered if research into documentation of the "
#| "issue has exhausted all sources.  This section provides a short "
#| "introduction into these which may be helpful for new Debian users."
msgid ""
"There are many sources of help, advice, and support for Debian users, but "
"these should only be considered if research into documentation of the issue "
"has exhausted all sources.  This section provides a short introduction to "
"these sources which may be helpful for new Debian users."
msgstr ""
"Debian 的使用者可以從許多的管道取得協助、建議、和支援，但您應該只有在用盡了各"
"種手段卻依然找不到問題的解答時才考慮尋求協助。這一節簡單的介紹了對 Debian 的"
"新使用者可能會很有幫助的求助管道。"

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "郵件論壇"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Debian 的使用者最感興趣的郵件論壇應該是 debian-user 論壇（英文）以及其他的 "
"debian-user-<replaceable>language</replaceable> 論壇（其他語言）了。您可以在"
"<ulink url=\"&url-debian-list-archives;\"></ulink> 取得這些論壇的相關細節，以"
"及訂閱的方法。請您在論壇中發言前先檢查過去的論壇存檔中是否已經提供了問題的解"
"答，並請遵守郵件論壇上的基本禮節。"

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "IRC(Internet Relay Chat) 網路聊天室"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
#, fuzzy
#| msgid ""
#| "Debian has an IRC channel dedicated to the support and aid of Debian "
#| "users located on the OFTC IRC network.  To access the channel, point your "
#| "favorite IRC client at irc.debian.org and join <literal>#debian</literal>."
msgid ""
"Debian has an IRC channel dedicated to the support and aid of Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian 在 OFTC IRC 上開設了一個 IRC 頻道，專門用來支援並幫助 Debian 的使用"
"者。您只要把您慣用的 IRC 客戶端程式指向 irc.debian.org 並加入 "
"<literal>#debian</literal> 就可以連上這個頻道了。"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
#, fuzzy
#| msgid ""
#| "Please follow the channel guidelines, respecting other users fully.  The "
#| "guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
#| "Wiki</ulink>."
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"請遵循各頻道的規範，並尊重線上其他的使用者。您可以在 <ulink url=\"&url-wiki;"
"DebianIRC\">Debian Wiki</ulink> 上取得這些規範。"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"可以在  <ulink url=\"&url-irc-host;\">website</ulink> 上取得更多 OFTC 的相關"
"資訊。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "回報錯誤"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
#, fuzzy
#| msgid ""
#| "We strive to make &debian; a high quality operating system, however that "
#| "does not mean that the packages we provide are totally free of bugs.  "
#| "Consistent with Debian's <quote>open development</quote> philosophy and "
#| "as a service to our users, we provide all the information on reported "
#| "bugs at our own Bug Tracking System (BTS).  The BTS is browseable at "
#| "<ulink url=\"&url-bts;\"></ulink>."
msgid ""
"We strive to make &debian; a high quality operating system; however that "
"does not mean that the packages we provide are totally free of bugs.  "
"Consistent with Debian's <quote>open development</quote> philosophy and as a "
"service to our users, we provide all the information on reported bugs at our "
"own Bug Tracking System (BTS).  The BTS is browseable at <ulink url=\"&url-"
"bts;\"></ulink>."
msgstr ""
"我們盡心盡力地讓 &debian; 成為一個高品質的作業系統，但這並不表示我們所提供的"
"套件都是完全沒有任何問題的。為了和 Debian 一貫的<quote>開放發展</quote>原則相"
"互呼應，也為了能對我們的使用者提供更好的服務，我們在我們自己的錯誤追蹤系統 "
"(BTS) 中提供了對所有報告過的錯誤的所有資訊。您可以在 <ulink url=\"&url-bts;"
"\">&url-bts;</ulink> 瀏覽錯誤追蹤系統的網站。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
#, fuzzy
#| msgid ""
#| "If you find a bug in the distribution or in packaged software that is "
#| "part of it, please report it so that it can be properly fixed for future "
#| "releases.  Reporting bugs requires a valid email address.  We ask for "
#| "this so that we can trace bugs and developers can get in contact with "
#| "submitters should additional information be needed."
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"如果您在我們的系統發行或其中的套件中發現了任何問題，請向我們回報，這樣我們才"
"能在日後的發行版中將它修復。您需要擁有一個正確的電子郵件信箱才能夠回報問題，"
"會有這個限制是因為這樣我們才能藉此追蹤錯誤報告的進展，而當開發人員需要更多資"
"訊的時候也才能和原問題報告者取得連繫。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
#, fuzzy
#| msgid ""
#| "You can submit a bug report using the program <command>reportbug</"
#| "command> or manually using email.  You can read more about the Bug "
#| "Tracking System and how to use it by reading the reference documentation "
#| "(available at <filename>/usr/share/doc/debian</filename> if you have "
#| "<systemitem role=\"package\">doc-debian</systemitem> installed) or online "
#| "at the <ulink url=\"&url-bts;\">Bug Tracking System</ulink>."
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can read more about the Bug Tracking System "
"and how to use it by reading the reference documentation (available at "
"<filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"您可以藉由 <command>reportbug</command> 程式或是自行使用電子郵件來進行回報錯"
"誤，您可以閱讀相關的參考文件（如果您有安裝 <systemitem role=\"package\">doc-"
"debian</systemitem> 的話，可以在 <filename>/usr/share/doc/debian</filename> "
"裡找到）或到 <ulink url=\"&url-bts;\">問題追蹤系統</ulink> 的線上網頁取得更多"
"如何使用錯誤追蹤系統及其它的相關資訊。"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "如何對 Debian 做出貢獻"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
#, fuzzy
#| msgid ""
#| "You do not need to be an expert to contribute to Debian.  By assisting "
#| "users with problems on the various user support <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> you are contributing to the community.  "
#| "Identifying (and also solving) problems related to the development of the "
#| "distribution by participating on the development <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
#| "Debian's high quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
#| "ulink> and help developers track them down and fix them.  If you have a "
#| "way with words then you may want to contribute more actively by helping "
#| "to write <ulink url=\"&url-ddp;\">documentation</ulink> or <ulink url="
#| "\"&url-debian-i18n;\">translate</ulink> existing documentation into your "
#| "own language."
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-svn-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"並不是只有箇中高手才能對 Debian 做出貢獻。當您在各個使用者 <ulink url=\"&url-"
"debian-list-archives;\">郵件論壇</ulink>中協助其他使用者解決問題時，您就是在"
"對整個社群做出貢獻。參與開發者 <ulink url=\"&url-debian-list-archives;\">郵件"
"論壇</ulink> 以協助找出（以及解決）和系統發行相關的問題對我們也有極大的幫助。"
"為了維護 Debian 系統發行一貫的高品質，請 <ulink url=\"&url-bts;\">回報問題</"
"ulink>，並協助開發人員找出問題的起因以修正錯誤。如果您有文字方面的天份，您也"
"可以選擇藉由 <ulink url=\"&url-ddp;\">文件</ulink> 的撰寫或把現有的文件 "
"<ulink url=\"&url-debian-i18n;\">翻譯</ulink> 成您所使用的語言來做出更直接的"
"貢獻。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
#, fuzzy
#| msgid ""
#| "If you can dedicate more time, you could manage a piece of the Free "
#| "Software collection within Debian.  Especially helpful is if people adopt "
#| "or maintain items that people have requested for inclusion within "
#| "Debian.  The <ulink url=\"&url-wnpp;\">Work Needing and Prospective "
#| "Packages database</ulink> details this information.  If you have an "
#| "interest in specific groups then you may find enjoyment in contributing "
#| "to some of Debian's subprojects which include ports to particular "
#| "architectures, <ulink url=\"&url-debian-jr;\">Debian Jr.</ulink> and "
#| "<ulink url=\"&url-debian-med;\">Debian Med</ulink>."
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"如果您能投注更多的時間的話，您可以負責維護 Debian 中一部分的自由軟體。如果您"
"能夠接續或維護其他使用者希望 Debian 所能提供的軟體的話就更好了，您可以在 "
"<ulink url=\"&url-wnpp;\">亟需人手以及受到期待的套件資料庫 (WNPP)</ulink> 中"
"取得相關的資訊。如果您的興趣是集中在特別的領域之中，那您可能會想要參加像是某"
"個特定硬體平台的系統移植、<ulink url=\"&url-debian-jr;\">Debian Jr.</ulink> "
"以及 <ulink url=\"&url-debian-med;\">Debian Med</ulink> 之類在 Debian 中的子"
"計畫。"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
#, fuzzy
#| msgid ""
#| "In any case, if you are working in the free software community in any "
#| "way, as a user, programmer, writer or translator you are already helping "
#| "the free software effort.  Contributing is rewarding and fun, and as well "
#| "as allowing you to meet new people it gives you that warm fuzzy feeling "
#| "inside."
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"不論如何，只要您以任何的方式參加了自由軟體社群的活動，不管是身為使用者、程式"
"員、作者、還是譯者，您就已經對自由軟體的群體努力做出貢獻了。貢獻本身就是一件"
"非常有益而有趣的事情，除了能讓您不斷地遇見新的伙伴之外，也能讓您的心中充滿了"
"溫暖的感覺。"
